import 'package:flutter/material.dart';

class BottomBarAppDemo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BottomBarAppDemoState();
}

class _BottomBarAppDemoState extends State<BottomBarAppDemo> {
  var _showFab = true;
  var _showNotch = true;
  var _fabLocation = FloatingActionButtonLocation.endDocked;
  void _onShowNotchChanged(bool value) {
    setState(() {
      _showNotch = value;
    });
  }

  void _onShowFabChanged(bool value) {
    setState(() {
      _showFab = value;
    });
  }

  void _onFabLocationChanged(FloatingActionButtonLocation value) {
    setState(() {
      _fabLocation = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bottom App Bar Demo"),
      ),
      body: ListView(
        padding: EdgeInsets.only(bottom: 88),
        children: <Widget>[
          SwitchListTile(
            value: _showFab,
            onChanged: _onShowFabChanged,
            title: Text("Show Floating Action Button"),
          ),
          SwitchListTile(
            value: _showNotch,
            onChanged: _onShowNotchChanged,
            title: Text("Show Notch"),
          ),
          Padding(
            padding: const EdgeInsets.all(16),
            child: Text("Bottom App Bar Position"),
          ),
          RadioListTile<FloatingActionButtonLocation>(
            title: Text("Docked End"),
            value: FloatingActionButtonLocation.endDocked,
            groupValue: _fabLocation,
            onChanged: _onFabLocationChanged,
          ),
          RadioListTile<FloatingActionButtonLocation>(
            title: Text("Docked Center"),
            value: FloatingActionButtonLocation.centerDocked,
            groupValue: _fabLocation,
            onChanged: _onFabLocationChanged,
          ),
          RadioListTile<FloatingActionButtonLocation>(
            title: Text("Floating End"),
            value: FloatingActionButtonLocation.endFloat,
            groupValue: _fabLocation,
            onChanged: _onFabLocationChanged,
          ),
          RadioListTile<FloatingActionButtonLocation>(
            title: Text("Floating Center"),
            value: FloatingActionButtonLocation.centerFloat,
            groupValue: _fabLocation,
            onChanged: _onFabLocationChanged,
          ),
        ],
      ),
      floatingActionButton: _showFab
          ? FloatingActionButton(
              onPressed: null,
              child: Icon(Icons.add),
              tooltip: "This is button Add, Click to do Nothing",
            )
          : null,
      floatingActionButtonLocation: _fabLocation,
      bottomNavigationBar: _DemoBottomAppBar(
        fabLocation: _fabLocation,
        shape: _showNotch ? CircularNotchedRectangle() : null,
      ),
    );
  }
}

class _DemoBottomAppBar extends StatelessWidget {
  final FloatingActionButtonLocation fabLocation;
  final NotchedShape shape;
  const _DemoBottomAppBar({this.fabLocation, this.shape});
  static final centerLocations = <FloatingActionButtonLocation>[
    FloatingActionButtonLocation.centerDocked,
    FloatingActionButtonLocation.centerFloat,
  ];
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
        shape: shape,
        child: IconTheme(
            data: IconThemeData(color: Theme.of(context).colorScheme.primary),
            child: Row(
              children: [
                IconButton(icon: const Icon(Icons.menu), onPressed: null),
                IconButton(icon: const Icon(Icons.search), onPressed: null),
                if (centerLocations.contains(fabLocation)) const Spacer(),
                IconButton(icon: const Icon(Icons.favorite), onPressed: null),
                IconButton(icon: const Icon(Icons.alarm), onPressed: null),
              ],
            )));
  }
}
