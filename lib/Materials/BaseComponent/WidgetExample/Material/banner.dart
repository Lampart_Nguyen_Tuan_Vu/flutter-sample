import 'package:flutter/material.dart';

enum BannerDemoAction {
  reset,
  showMultipleActions,
  showLeading,
}

class BannerDemo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BannerDemoState();
}

class _BannerDemoState extends State<BannerDemo> {
  static const _itemCount = 20;
  var _displayBanner = true;
  var _showMultipleActions = true;
  var _showLeading = true;

  void handleDemoAction(BannerDemoAction action) {
    setState(() {
      switch (action) {
        case BannerDemoAction.reset:
          _displayBanner = true;
          _showMultipleActions = true;
          _showLeading = true;
          break;
        case BannerDemoAction.showMultipleActions:
          _showMultipleActions = !_showMultipleActions;
          break;
        case BannerDemoAction.showLeading:
          _showLeading = !_showLeading;
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final colorScheme = Theme.of(context).colorScheme;
    final banner = MaterialBanner(
      content: Text('This is Banner'),
      leading: _showLeading
          ? CircleAvatar(
              child: Icon(Icons.access_alarm, color: colorScheme.primary))
          : null,
      actions: [
        FlatButton(
            onPressed: () {
              setState(() {
                _displayBanner = false;
              });
            },
            child: Text("SIGN IN")),
        if (_showMultipleActions)
          FlatButton(
              onPressed: () {
                setState(() {
                  _displayBanner = false;
                });
              },
              child: Text("DISMISS")),
      ],
      backgroundColor: colorScheme.background,
    );

    return Scaffold(
        appBar: AppBar(
          title: Text("Demo Banner"),
          actions: [
            PopupMenuButton<BannerDemoAction>(
              onSelected: handleDemoAction,
              itemBuilder: (context) => <PopupMenuEntry<BannerDemoAction>>[
                PopupMenuItem<BannerDemoAction>(
                    value: BannerDemoAction.reset, child: Text("Reset Banner")),
                CheckedPopupMenuItem<BannerDemoAction>(
                    value: BannerDemoAction.showLeading,
                    child: Text("Show Leading")),
                PopupMenuItem<BannerDemoAction>(
                    value: BannerDemoAction.showMultipleActions,
                    child: Text("Show Multiple Actions")),
              ],
            ),
          ],
        ),
        body: ListView.builder(
          itemBuilder: (context, index) {
            if (index == 0 && _displayBanner) return banner;
            return ListTile(
              title: Text("ListView Item" + index.toString()),
            );
          },
          itemCount: _displayBanner ? _itemCount + 1 : _itemCount,
        ));
  }
}
