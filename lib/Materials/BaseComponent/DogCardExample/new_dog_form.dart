import 'package:examples/Models/dog_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddDogFormPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AddDogFormPageState();
}

class _AddDogFormPageState extends State<AddDogFormPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController locController = TextEditingController();
  TextEditingController desController = TextEditingController();
  void submitPub(BuildContext context) {
    if (nameController.text.isEmpty) {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.redAccent,
          content: Text('Pups neeed names!'),
        ),
      );
    } else {
      var newDog =
          Dog(nameController.text, locController.text, desController.text);
      Navigator.of(context).pop(newDog);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
          AppBar(backgroundColor: Colors.black87, title: Text('Add a new Dog')),
      body: Container(
        color: Colors.black54,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 32),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                    labelText: 'Name the Pup',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: TextField(
                  controller: locController,
                  decoration: InputDecoration(
                    labelText: "Pup's location",
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: TextField(
                  controller: desController,
                  decoration: InputDecoration(
                    labelText: 'All about the Pup',
                  ),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Builder(builder: (context) {
                    return RaisedButton(
                      child: Text('Submit Pup'),
                      onPressed: () => submitPub(context),
                      color: Colors.indigoAccent,
                    );
                  })),
            ],
          ),
        ),
      ),
    );
  }
}
