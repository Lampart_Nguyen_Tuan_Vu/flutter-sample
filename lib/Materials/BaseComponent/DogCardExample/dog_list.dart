import 'package:examples/Materials/BaseComponent/DogCardExample/dog_card.dart';
import 'package:examples/Models/dog_model.dart';
import 'package:flutter/cupertino.dart';

class DogList extends StatelessWidget {
  final List<Dog> doggos;
  DogList(this.doggos);
  @override
  Widget build(BuildContext context) {
    return _buildList(context);
  }

  ListView _buildList(context) {
    return ListView.builder(
      itemCount: doggos.length,
      itemBuilder: (context, int) {
        return DogCard(doggos[int]);
      },
    );
  }
}
