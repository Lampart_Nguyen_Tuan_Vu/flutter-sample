import 'package:examples/Models/dog_model.dart';
import 'package:flutter/material.dart';

import 'dog_list.dart';
import 'new_dog_form.dart';

class DogMainPage extends StatefulWidget {
  DogMainPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _DogMainPageState createState() => _DogMainPageState();
}

class _DogMainPageState extends State<DogMainPage> {
  List<Dog> initialDoggos = []
    ..add(Dog('Ruby', 'Portland, OR, USA',
        'Ruby is a very good girl. Yes: Fetch, loungin\'. No: Dogs who get on furniture.'))
    ..add(Dog('Rex', 'Seattle, WA, USA', 'Best in Show 1999'))
    ..add(Dog('Rod Stewart', 'Prague, CZ',
        'Star good boy on international snooze team.'))
    ..add(Dog('Herbert', 'Dallas, TX, USA', 'A Very Good Boy'))
    ..add(Dog('Buddy', 'North Pole, Earth', 'Self proclaimed human lover.'));

  Future _showNewDogForm() async {
    Dog newDog = await Navigator.of(context)
        .push(MaterialPageRoute(builder: (BuildContext context) {
      return AddDogFormPage();
    }));

    if (newDog != null) {
      initialDoggos.add(newDog);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: _showNewDogForm,
          )
        ],
      ),
      body: Container(
        child: Center(
          child: DogList(initialDoggos),
        ),
        decoration: BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Colors.indigo[900],
            Colors.indigo[700],
            Colors.indigo[500],
            Colors.indigo[300],
          ],
          stops: [0.1, 0.5, 0.7, 0.9],
        )),
      ),
    );
  }
}
