import 'package:examples/Materials/BaseComponent/DogCardExample/dog_detail_page.dart';
import 'package:examples/Models/dog_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DogCard extends StatefulWidget {
  final Dog dog;

  DogCard(this.dog);

  @override
  _DogCardState createState() => _DogCardState(dog);
}

class _DogCardState extends State<DogCard> {
  Dog dog;
  String renderUrl;
  _DogCardState(this.dog);

  Widget get dogImage {
    var dogAvatar = Hero(
        tag: dog,
        child: Container(
          width: 100,
          height: 100,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              fit: BoxFit.cover,
              image: NetworkImage(renderUrl ?? ''),
            ),
          ),
        ));

    var placeholder = Container(
      width: 100,
      height: 100,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        gradient: LinearGradient(
          colors: [Colors.black87, Colors.black12, Colors.blueGrey],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
      ),
      alignment: Alignment.center,
      child: Text(
        'DOGGO',
        textAlign: TextAlign.center,
      ),
    );

    return AnimatedCrossFade(
        firstChild: placeholder,
        secondChild: dogAvatar,
        crossFadeState: renderUrl == null
            ? CrossFadeState.showFirst
            : CrossFadeState.showSecond,
        duration: Duration(milliseconds: 1000));
  }

  Widget get dogCard {
    return Container(
      width: 290,
      height: 115,
      child: Card(
          color: Colors.black87,
          child: Padding(
              padding: const EdgeInsets.only(
                top: 8,
                bottom: 8,
                left: 64,
                right: 64,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    widget.dog.name,
                    style: Theme.of(context).textTheme.headline,
                  ),
                  Text(
                    widget.dog.location,
                    style: Theme.of(context).textTheme.subhead,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.star,
                      ),
                      Text(':${widget.dog.rating}/10'),
                    ],
                  )
                ],
              ))),
    );
  }

  void initState() {
    super.initState();
    renderDocPic();
  }

  void renderDocPic() async {
    await dog.getImageUrl();
    if (mounted) {
      setState(() {
        renderUrl = dog.imageUrl;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: showDogDetailPage,
      child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child: Container(
            height: 115,
            child: Stack(
              children: <Widget>[
                Positioned(
                  child: dogCard,
                  left: 60,
                ),
                Positioned(
                  child: dogImage,
                  top: 7.5,
                  left: 10,
                )
              ],
            ),
          )),
    );
  }

  showDogDetailPage() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return DogDetailPage(dog);
    }));
  }
}
