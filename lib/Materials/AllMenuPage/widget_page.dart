import 'package:examples/Materials/AllMenuPage/animation_item.dart';
import 'package:examples/Materials/BaseComponent/WidgetExample/Material/banner.dart';
import 'package:examples/Materials/BaseComponent/WidgetExample/Material/bottom_navigation.dart';
import 'package:examples/Materials/BaseComponent/WidgetExample/Material/bottom_sheet.dart';
import 'package:examples/Materials/BaseComponent/WidgetExample/Material/botttom_app_bar.dart';
import 'package:examples/Materials/BaseComponent/WidgetExample/Material/card.dart';
import 'package:examples/Materials/BaseComponent/WidgetExample/Material/chip.dart';
import 'package:examples/Materials/BaseComponent/WidgetExample/Material/data_table.dart';
import 'package:examples/Materials/BaseComponent/WidgetExample/Material/dialog.dart';
import 'package:flutter/material.dart';

class WidgetPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _WidgetPageState();
}

class _WidgetPageState extends State<WidgetPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black87,
          title: Text('List of Widget Example'),
        ),
        body: ListView(
          children: <Widget>[
            InkWell(
              child: AnimationItem('Banner Demo'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return BannerDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Bottom App Bar Demo'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return BottomBarAppDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Bottom Navigation Demo'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return BottomNavigationDemo(
                    type: BottomNavigationDemoType.withLabels,
                  );
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Bottom Sheet Demo'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return BottomSheetDemo(
                    type: BottomSheetDemoType.persistent,
                  );
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Cards Demo'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return CardsDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Chips Demo'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return ChipDemo(
                    type: ChipDemoType.action,
                  );
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Table Demo'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return DataTableDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Dialog Demo'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return DialogDemo(
                    type: DialogDemoType.simple,
                  );
                }))
              },
            ),
          ],
        ));
  }
}
