import 'package:examples/Materials/AllMenuPage/animation_item.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/AnimatedBuilder.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/AnimatedContainer/AnimatedContainer.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/AnimatedList.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/AnimatedPositioned.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/AnimatedSwitcher.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/AnimationController.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/CardSwipe.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/Carousel.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/CuctomTween.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/ExpandCard.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/FadeTransition.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/FocusImage.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/HeroAnimation.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/PageRouterBuilder.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/PegProgress/peg_progress_indicator_container.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/PhysicCardDrag.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/RepeatingAnimation.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/Tween.dart';
import 'package:examples/Materials/BaseComponent/AnimationExample/TweenSequence.dart';
import 'package:flutter/material.dart';

class AnimationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _animationState();
}

class _animationState extends State<AnimationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black87,
          title: Text('List of Animation Example'),
        ),
        body: ListView(
          children: <Widget>[
            InkWell(
              child: AnimationItem('Peg Animation'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return PegProgressIndicator();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Animation Container'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return AnimatedContainerDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Page Router'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return PageRouteBuilderDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Animation Controller'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return AnimationControllerDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Tween'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return TweenDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Animated Builder'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return AnimatedBuilderDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Custom Tween'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return CustomTweenDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Tween Sequence'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return TweenSequenceDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Fade Transition'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return FadeTransitionDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Animated Switcher'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return AnimatedSwitcherDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Animated Positioned'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return AnimatedPositionedDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Animated List'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return AnimatedListDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Card Swipe'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return CardSwipeDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Carousel'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return CarouselDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Expand Card'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return ExpandCardDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Focus Image'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return FocusImageDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Hero Animation'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return HeroAnimationDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Physic Card Drag'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return PhysicsCardDragDemo();
                }))
              },
            ),
            InkWell(
              child: AnimationItem('Repeating Animation'),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return RepeatingAnimationDemo();
                }))
              },
            ),
          ],
        ));
  }
}
