import 'package:flutter/material.dart';

class AnimationItem extends StatefulWidget {
  String name;
  AnimationItem(this.name);
  @override
  State<StatefulWidget> createState() => _animationItemState();
}

class _animationItemState extends State<AnimationItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
        child: Text(
          widget.name,
          style: Theme.of(context).textTheme.subtitle,
        ),
      ),
    );
  }
}
