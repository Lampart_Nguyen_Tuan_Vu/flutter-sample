import 'package:examples/Materials/AllMenuPage/animation_page.dart';
import 'package:examples/Materials/AllMenuPage/widget_page.dart';
import 'package:examples/Materials/BaseComponent/DogCardExample/dog_main.dart';
import 'package:examples/Materials/BaseComponent/WebViewExample/WebviewExample.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        brightness: Brightness.dark,
      ),
      home: MenuView(title: 'Flutter Demo Home Page'),
    );
  }
}

class MenuView extends StatefulWidget {
  MenuView({Key key, this.title}) : super(key: key);

  final String title;
  @override
  State<StatefulWidget> createState() => _MenuViewState();
}

class _MenuViewState extends State<MenuView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: ListView(
          children: <Widget>[
            InkWell(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Text('Widget Example'),
              ),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return WidgetPage();
                }))
              },
            ),
            InkWell(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Text('List Item Example'),
              ),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return DogMainPage(title: 'Flutter Demo Home Page');
                }))
              },
            ),
            InkWell(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Text('Animation Example'),
              ),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return AnimationPage();
                }))
              },
            ),
            InkWell(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Text('WebView Example'),
              ),
              onTap: () => {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return WebViewDemo();
                }))
              },
            )
          ],
        ));
  }
}
